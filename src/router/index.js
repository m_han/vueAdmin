// 导入组件
import Vue from 'vue';
import Router from 'vue-router';
// 登录
import login from '@/views/login';
// 首页
import index from '@/views/index';
/**
 * 商城管理
 */
// 社区团购管理
import Goods from '@/views/goods/Goods';
// 今日拼单管理
import ordertoday from "@/views/goods/ordertoday";
// 限时秒杀管理
import timelimit from "@/views/goods/timelimit";
// 首页管理
import First from '@/views/goods/First';


// 积分商品管理
import integral from "@/views/integral/integral";
// 审核中心
import auditcenter from "@/views/audit/auditcenter";
/**
 * 订单管理
 */
// 社区团购订单
import Order from '@/views/order/Order';
// 今日拼单订单
import porder from "@/views/order/porder";
// 综合商家订单
import zorder from "@/views/order/zorder";
// 外卖订单
import worder from "@/views/order/worder";
/**
 * 系统管理
 */
// 账户
import Module from '@/views/system/Module';
// 角色
import Role from '@/views/system/Role';
// 菜单
import Dept from '@/views/system/Dept';
// 短信设置
import Variable from '@/views/system/Variable';
// 关于系统
import Permission from '@/views/system/Permission';
// APP版本管理
import Edition from "@/views/system/Edition";
/**
  * 用户管理
*/
// 普通用户
import user from '@/views/system/user';
// 普通商家
import store from "@/views/system/store";
// 连锁总部商家
import chainstore from "@/views/system/chainstore";
/**
 * 内容管理
 */
// 系统通知
import notice from "@/views/contents/notice";
// 店铺分类管理
import shopsort from "@/views/contents/shopsort";
// 优惠券设置
import coupon from "@/views/contents/coupon";
// 邀请新用户
import inviteuser from "@/views/contents/inviteuser";
/**
 * 资金管理
 */
// 提现列表
import MachineConfig from '@/views/machine/MachineConfig';
// 提现设置
import Config from '@/views/machine/Config';
// 平台财务流水
import Machine from '@/views/machine/Machine';
// 用户财务流水
import MachineAisle from '@/views/machine/MachineAisle';


// 内容服务文件
import contservice from "@/views/contservice/contservice";
/**
 * 客服
 */
// 客服
import customerService from '@/views/customer_service/customer_service';

// 图表界面
import statistics from '@/views/charts/statistics';

// 启用路由
Vue.use(Router);

// 导出路由
export default new Router({
    routes: [{
        path: '/',
        name: '',
        component: login,
        hidden: true,
        meta: {
            requireAuth: false
        }
    }, {
        path: '/login',
        name: '登录',
        component: login,
        hidden: true,
        meta: {
            requireAuth: false
        }
    }, {
        path: '/index',
        name: '首页',
        component: index,
        iconCls: 'el-icon-tickets',
        children: [
          {
              path: '/charts/statistics',
              name: '数据可视化',
              component: statistics,
              meta: {
                  requireAuth: true
              }
          }, {
            path: '/goods/Goods',
            name: '社区团购管理',
            component: Goods,
            meta: {
                requireAuth: true
            }
        },{
            path: '/goods/ordertoday',
            name: '今日拼单管理',
            component: ordertoday,
            meta: {
                requireAuth: true
            }
        },{
            path: '/goods/timelimit',
            name: '限时秒杀管理',
            component: timelimit,
            meta: {
                requireAuth: true
            }
        },{
            path: '/goods/First',
            name: '首页管理',
            component: First,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/integral/integral',
            name: '积分商品管理',
            component: integral,
            meta: {
                requireAuth: true
            }
        },{
            path: '/audit/auditcenter',
            name: '审核中心',
            component: auditcenter,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/order/Order',
            name: '社区团购订单',
            component: Order,
            meta: {
                requireAuth: true
            }
        },{
            path: '/order/porder',
            name: '今日拼单订单',
            component: porder,
            meta: {
                requireAuth: true
            }
        },{
            path: '/order/zorder',
            name: '综合商家订单',
            component: zorder,
            meta: {
                requireAuth: true
            }
        },{
            path: '/order/worder',
            name: '外卖订单',
            component: worder,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/system/user',
            name: '普通用户',
            component: user,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/system/store',
            name: '普通商家',
            component: store,
            meta: {
                requireAuth: true
            }
        },{
            path: '/system/chainstore',
            name: '连锁总部商家',
            component: chainstore,
            meta: {
                requireAuth: true
            }
        },{
            path: '/contents/notice',
            name: '系统通知',
            component: notice,
            meta: {
                requireAuth: true
            }
        },{
            path: '/contents/shopsort',
            name: '店铺分类管理',
            component: shopsort,
            meta: {
                requireAuth: true
            }
        },{
            path: '/contents/coupon',
            name: '优惠券设置',
            component: coupon,
            meta: {
                requireAuth: true
            }
        },{
            path: '/contents/inviteuser',
            name: '邀请新用户',
            component: inviteuser,
            meta: {
                requireAuth: true
            }
        },{
            path: '/system/Module',
            name: '账户',
            component: Module,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/system/Role',
            name: '角色管理',
            component: Role,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/system/Dept',
            name: '菜单',
            component: Dept,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/system/Variable',
            name: '短信设置',
            component: Variable,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/system/Permission',
            name: '关于系统',
            component: Permission,
            meta: {
                requireAuth: true
            }
        },{
            path: '/system/Edition',
            name: 'APP版本管理',
            component: Edition,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/machine/MachineConfig',
            name: '提现列表',
            component: MachineConfig,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/machine/Config',
            name: '提现设置',
            component: Config,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/machine/Machine',
            name: '平台财务流水',
            component: Machine,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/machine/MachineAisle',
            name: '用户财务流水',
            component: MachineAisle,
            meta: {
                requireAuth: true
            }
        },{
            path: '/contservice/contservice',
            name: '内容服务文件',
            component: contservice,
            meta: {
                requireAuth: true
            }
        }, {
            path: '/customer_service/customer_service',
            name: '客服',
            component: customerService,
            meta: {
                requireAuth: true
            }
        }]
    }]
})

// 禁止重复跳转报错
const VueRouterPush = Router.prototype.push
Router.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}
