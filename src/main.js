// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// 引入element UI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
// 引入路由
import router from './router'
// 引入状态管理
import store from './vuex/store'
// 引入icon
import './assets/icon/iconfont.css'

import { Message } from 'element-ui'


// 引入echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
import axios from 'axios'
Vue.prototype.$axios = axios

Vue.config.productionTip = false

// 使用element UI
Vue.use(ElementUI)
// 过滤器
import * as custom from './utils/util'

Object.keys(custom).forEach(key => {
    Vue.filter(key, custom[key])
})

// 判断是开发环境还是线上环境
var baseURLStr="http://malladmin.micromove.cn"
// var baseURLStr="http://192.168.10.24:8083";
if(process.env.NODE_ENV=="development"){
  axios.defaults.baseURL = '/api'
}else if(process.env.NODE_ENV=="production"){
  axios.defaults.baseURL = baseURLStr
}

// 请求拦截器
axios.interceptors.request.use(
  config => {
    if(config.method === "get" || config.method === "post" || config.method === "delete"){
      config.headers["Authorization"]="Bearer "+localStorage.getItem("logintoken")
      config.headers["Content-Type"]="application/json;charset=utf-8"
    }
    return config
  }
)

// 响应拦截器
axios.interceptors.response.use(
  response => {
    // 判断code码==401的时候，说明登录过期，重新去登录
    if(response.data.code==401){
      store.commit('logout', 'false')
      router.replace({ path: '/' })
      Message.error("登录超时，请重新登录！")
    }
    return response
  },
  error => {
    return Promise.reject(error)
  }
)
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store, //使用store vuex状态管理
    components: { App },
    template: '<App/>',
    data: {
        // 空的实例放到根组件下，所有的子组件都能调用
        Bus: new Vue()
    }

})
