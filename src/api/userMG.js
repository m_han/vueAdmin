import axios from 'axios';
import {
  loginreq,
  req,
  putdata,
} from './axiosFun';
// login
// 登录接口
export const login = (params) => {
  return axios.post("/auth/login", params).then(res=>res)
};

// components->leftnav
// 获取左侧权限列表
export const powerlist=()=>{
  return req("get","/system/menu/findAll")
}




// system->user
// 获取普通用户列表
export const plainuser = (params)=>{
  return req("get","/identity/mem/findMem?pageNum="+params.pagenum+"&memberName="+params.membername)
};
// 编辑普通角色
export const changeplainuser=(params)=>{
  return axios({
    url: "/identity/mem/updateMem",
    method: "put",
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/x-www-form-urlencoded'
    },
    transformRequest: [
      function(oldData){
        // console.log(oldData)
        let newStr = ''
        for (let item in oldData){
          newStr += encodeURIComponent(item) + '=' + encodeURIComponent(oldData[item]) + '&'
        }
        newStr = newStr.slice(0, -1)
        return newStr
      }
    ],
  }).then(res=>res.data)
}



// system->store
// 获取普通商家列表
export const plainstore=(params)=>{
  return req("get","/store/store/findStore?pageNum="+params.pagenum+"&StoreName="+params.storename+"&StoreTel="+params.storephone+"&StoreState="+params.state)
}
// 审核普通商家
export const changeplainstore=(params)=>{
  return putdata("/store/store/auditStore",params)
}
// 根据店铺id查询店铺详情
export const chaplainstore=(params)=>{
  return req("get","/store/store/queryStore?storeId="+params.storeid)
}
// 根据总店查询店下所有分店
export const chainshop=(params)=>{
  return req("get","/store/store/findRel?storeId="+params.storeid)
}
// 给总店添加分店
export const addfendian=(params)=>{
  return req("post","/store/store/insertRel?name=root",params)
}
// 总店解除分店关系
export const jiefendian=(params)=>{
  return req("delete","/store/store/deleteRel?name=root",params)
}
// 编辑店铺是否首页推荐
export const shouyetuijian=(params)=>{
  return axios({
    url: "/store/store/recommended",
    method: "put",
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/x-www-form-urlencoded'
    },
    transformRequest: [
      function(oldData){
        // console.log(oldData)
        let newStr = ''
        for (let item in oldData){
          newStr += encodeURIComponent(item) + '=' + encodeURIComponent(oldData[item]) + '&'
        }
        newStr = newStr.slice(0, -1)
        return newStr
      }
    ],
  }).then(res=>res.data)
}






// system->Permission
// 编辑关于微动商城
export const permission = (params)=>{
  return putdata("/system/system/updateSys",params)
}
// 查询关于微动商城
export const permissionc = () =>{
  return req("get","/system/system/list")
}



// system->Edition
// 查询app版本记录
export const editionlist=(params)=>{
  return req("get","/system/version/findPageVersion?differentiate="+params.diff+"&pageNum="+params.pagenum)
}
// 添加app版本
export const addedition=(params)=>{
  return req("post","/system/version/insertVersion?name=root",params)
}
// 修改app版本上下架
export const updownedition=(params)=>{
  return putdata("/system/version/editApp",params)
}



// system->Variable
// 查询系统变量
export const chabianliang=(params)=>{
  return req("get","/system/config/findConfigPage?pageNum="+params.pagenum+"&configName="+params.name)
}
// 新增系统变量
export const addbianliang=(params)=>{
  return req("post","/system/config/addConfig?name=admin",params)
}
// 修改系统变量
export const changebianliang=(params)=>{
  return putdata("/system/config/editConfig?name=admin",params)
}







// goods->First
// 上传图片
export const uploadimg=(params)=>{
  return axios({method: 'post',url: '/file/upload',headers: {'Authorization': 'Bearer '+localStorage.getItem("logintoken"),},data: params}).then(res=>res)
}
// 新增banner
export const addbanner=(params)=>{
  return req("post",'/member/banner/add?name=root',params)
}
// 查询banner
export const searchbanner=(params)=>{
  return req("get","/member/banner/findBanner?pageNum="+params)
}
// 修改banner
export const changeBanner=(params)=>{
  return putdata("/member/banner/edit",params)
}
// 删除banner
export const deleteBanner=(params)=>{
  return req("delete","/member/banner/rmBanner",params)
}
// 新增功能区分类
export const addfunc=(params)=>{
  return req("post","/system/type/addFun?name=admin",params)
}
// 查询功能区分类
export const chafunc=(params)=>{
  return req("get","/system/type/findFunPage?pageNum="+params.pagenum+"&typeName="+params.typename)
}
// 编辑修改功能区分类
export const changefunc=(params)=>{
  return putdata("/system/type/editFun",params)
}
// 删除功能区分类
export const deletefunc=(params)=>{
  return req("delete","/system/type/removeFun",params)
}







// contents->notice
// 新增系统通知
export const addservice=(params)=>{
  return req("post","/system/info/addInfo?name=root",params)
}
// 查询系统通知
export const chaservice=(params)=>{
  return req("get","/system/info/findInfo?pageNum="+params.pagenum+"&title="+params.title)
}
// 删除系统通知
export const deleteservice=(params)=>{
  return req("delete","/system/info/deleteInfo",params)
}



// system->Role
// 新增角色
export const adduser=(params)=>{
  return req("post","/system/role/addRole",params)
}
// 查询角色信息
export const chauser=(params)=>{
  return req("get","/system/role/findRolePage?roleName="+params.name+"&pageNum="+params.pagenum)
}
// 删除角色
export const deleteuser=(params)=>{
  return req("delete","/system/role/remove/"+params)
}
// 修改角色
export const changeuser=(params)=>{
  return putdata("/system/role/editRole",params)
}
// 查询菜单列表
export const chacaidan=()=>{
  return req("get","/system/menu/findAll")
}



// contservice->contservice
// 新增内容服务文件
export const addcontservice=(params)=>{
  return req("post","/system/server/add?name=admin",params)
}
// 查询内容服务文件
export const chacontservice=(params)=>{
  return req("get","/system/server/findPageContent?pageNum="+params.pagenum+"&title="+params.title)
}
// 编辑修改内容服务文件
export const changecontservice=(params)=>{
  return putdata("/system/server/edit?name=root",params)
}
// 删除内容服务文件
export const deletecontservice=(params)=>{
  return req("delete","/system/server/delete",params)
}




// systen->Module
// 获取角色列表
export const huoqvjuese=()=>{
  return req("get","/system/role/findRoleAll")
}
// 新增系统账户
export const addaccount=(params)=>{
  return req("post","/system/user/addUser",params)
}
// 查询系统账户
export const chaaccount=(params)=>{
  return req("get","/system/user/findSystem?pageNum="+params.pagenum+"&userName="+params.username+"&phoneNumber="+params.phone)
}
// 修改系统账户密码
export const changeaccountpass=(params)=>{
  return putdata("/system/user/resetPwd",params)
}
// 编辑系统用户
export const changeaccount=(params)=>{
  return putdata("/system/user/editUser",params)
}
// 删除系统用户
export const deleteaccount=(params)=>{
  return req("delete","/system/user/userIds",params)
}




// system->Dept
// 新增系统菜单
export const addmenu=(params)=>{
  return req("post","/system/menu/addMenu",params)
}
// 查询系统菜单
export const chamenu=(params)=>{
  return req("get","/system/menu/findMenuPage?pageNum="+params.pagenum+"&menuName="+params.menuname)
}
// 编辑修改系统菜单
export const changemenu=(params)=>{
  return putdata("/system/menu/editMenu",params)
}
// 删除系统菜单
export const deletemenu=(params)=>{
  return req("delete","/system/menu/"+params)
}




// system->coupon
// 新增优惠券
export const addcoupon=(params)=>{
  return req("post","/store/platcoupons/insertPlat",params)
}
// 查询优惠券
export const chacoupon=(params)=>{
  return req("get","/store/platcoupons/findPlat?pageNum="+params.pagenum+"&cpTitle="+params.title)
}
// 编辑优惠券
export const changecoupon=(params)=>{
  return putdata("/store/platcoupons/updatePlat",params)
}
// 删除优惠券
export const deletecoupon=(params)=>{
  return req("delete","/store/platcoupons/deletePlat",params)
}



// contents->shopsort
// 新增店铺分类
export const addshopsort=(params)=>{
  return req("post","/store/category/addCategory",params)
}
// 查询店铺分类
export const chashopsort=(params)=>{
  return req("get","/store/category/findCategoryPage?pageNum="+params.pagenum)
}
// 编辑店铺分类
export const changeshopsort=(params)=>{
  return putdata("/store/category/editCategory",params)
}














// goods->Goods
// 新增 -> 团购类别
export const addgroupbuy=(params)=>{
  return req("post","/member/group/add?name=root",params)
}
// 查询 -> 团购类别
export const chagroupbuy=(params)=>{
  return req("get","/member/group/findGroup?groupName="+params.groupname+"&pageNum="+params.pagenum+"&type="+params.type)
}
// 编辑 -> 团购类别
export const changegroupbuy=(params)=>{
  return putdata("/member/group/editGroup",params)
}
// 删除 -> 团购类别
export const deletegroupbuy=(params)=>{
  return req("delete","/member/group/deleteGroup",params)
}
// 修改团购普通商品状态
export const changezhuangtai=(params)=>{
  return putdata("/goods/spu/auditGoods",params)
}
// 获取团购社区信息
export const chaheader=(params)=>{
  return req("get","/member/header/findHeader?pageNum="+params.pagenum+"&comName="+params.comname+"&comState="+params.comstate)
}
// 修改社区团长状态
export const changeheader=(params)=>{
  return putdata("/member/header/audit",params)
}
// 编辑社区二维码
export const headerweima=(params)=>{
  return putdata("/member/header/updateHeader",params)
}

// 设为首页广告商品
export const shouyehuodong=(params)=>{
  return req("post","/system/acivtiy/addActivityGoods",params)
}
// 查询首页广告商品
export const chashouye=(params)=>{
  return req("get","/system/acivtiy/findPageActivity?sellType="+params.type+"&pageNum="+params.pagenum)
}


// 删除广告商品
export const deleteguanggao=(params)=>{
  return req("delete","/system/acivtiy/delActivity",params)
}











// integral->integral
// 新增积分商品分类
export const addsortjifen=(params)=>{
  return req("post","/member/intcategory/add?name=root",params)
}
// 查询积分商品分类
export const chasortjifen=(params)=>{
  return req("get","/member/intcategory/findIntCat?intName="+params.shopname+"&pageNum="+params.pagenum)
}
// 修改积分商品分类
export const changesortjifen=(params)=>{
  return putdata("/member/intcategory/edit",params)
}
// 删除积分商品分类

// 获取积分商品列表
export const chalistjifen=(params)=>{
  return req("get","/member/intgood/findGoods?pageNum="+params.pagenum+"&goodName="+params.goodname+"&goodId="+params.goodid+"&sendPhone="+params.sendphone)
}
// 审核积分商品
export const shenhejifen=(params)=>{
  return putdata("/member/intgood/auditGoods",params)
}



// goods->timelimit
// 获取限时秒杀商品列表
export const miaoshalist=(params)=>{
  return req("get","/search/esSpu/findKillGoods?pageNum="+params.pagenum+"&spuName="+params.spuname+"&storeName="+params.storename)
}




// audit->auditcenter
// 查询店铺动态
export const chastorenews=(params)=>{
  return req("get","/store/snstracelog/findStoreSns?pageNum="+params.pagenum+"&straceStorename="+params.storename+"&straceState="+params.status)
}
// 审核店铺动态
export const shenstorenews=(params)=>{
  return putdata("/store/snstracelog/auditStoreSons",params)
}










// 新增 -> 商品列表
export const addshoplist=(params)=>{
  return req("")
}
// 查询 -> 商品列表
export const chashoplist=(params)=>{
  return req("get","/search/esSpu/findSpuGoods?pageNum="+params.pagenum+"&storeName="+params.storename+"&skilled="+params.skilled+"&sellType="+params.selltype+"&spuName="+params.spuname)
}



// machine->Machine
// 获取平台收入列表
export const pingtaishou=(params)=>{
  return req("get","/order/withdrawal/findMvStoreApp?startTime="+params.start+"&endTime="+params.end+"&pageNum="+params.pagenum)
}
// 获取平台支出列表
export const pingtaizhi=(params)=>{
  return req("get","/order/headewithdrawal/findMvHeadApp?startTime="+params.start+"&endTime="+params.end+"&pageNum="+params.pagenum)
}



// machina->MachineAisle
// 获取团长财务流水
export const tuanzhangcai=(params)=>{
  return req("get","/order/headewithdrawal/findIncExp?pageNum="+params.pagenum+"&MemberName="+params.name)
}
// 获取商铺财务流水
export const storecai=(params)=>{
  return req("get","/order/withdrawal/findWithdrawalLog?pageNum="+params.pagenum)
}




// machine->MachineConfig
// 获取团长提现列表
export const tuantixian=(params)=>{
  return req("get","/order/headewithdrawal/findMemWithdrawal?TranState="+params.state+"&MemberName="+params.name+"&pageNum="+params.pagenum)
}
// 审核团长提现成功
export const tuantixiancheng=(params)=>{
  return axios({
    url: "/order/headewithdrawal/makeWithdrawal",
    method: "put",
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/x-www-form-urlencoded'
    },
    transformRequest: [
      function(oldData){
        // console.log(oldData)
        let newStr = ''
        for (let item in oldData){
          newStr += encodeURIComponent(item) + '=' + encodeURIComponent(oldData[item]) + '&'
        }
        newStr = newStr.slice(0, -1)
        return newStr
      }
    ],
  }).then(res=>res.data)
}
// 审核团长提现失败
export const tuantixianno=(params)=>{
  return axios({
    url: "/order/headewithdrawal/makeNotWithdrawal",
    method: "put",
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/x-www-form-urlencoded'
    },
    transformRequest: [
      function(oldData){
        // console.log(oldData)
        let newStr = ''
        for (let item in oldData){
          newStr += encodeURIComponent(item) + '=' + encodeURIComponent(oldData[item]) + '&'
        }
        newStr = newStr.slice(0, -1)
        return newStr
      }
    ],
  }).then(res=>res.data)
}


// 获取店铺提现列表
export const storetixian=(params)=>{
  return req("get","/order/withdrawal/findWithdrawal?TranState="+params.state+"&StoreName="+params.name+"&pageNum="+params.pagenum)
}
// 审核店铺提现成功
export const storetixiancheng=(params)=>{
  return axios({
    url: "/order/withdrawal/makeWithdrawal",
    method: "put",
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/x-www-form-urlencoded'
    },
    transformRequest: [
      function(oldData){
        // console.log(oldData)
        let newStr = ''
        for (let item in oldData){
          newStr += encodeURIComponent(item) + '=' + encodeURIComponent(oldData[item]) + '&'
        }
        newStr = newStr.slice(0, -1)
        return newStr
      }
    ],
  }).then(res=>res.data)
}
// 审核店铺提现失败
export const storetixianno=(params)=>{
  return putdata("/order/withdrawal/makeNoWithdrawal",params)
}





// order->Order
// 获取订单列表 0-已取消 1-未付款 2-已付款 4-已发货 5-已收货
export const orderlist=(params)=>{
  return req("get","/search/order/findPageOrder?sellType="+params.type+"&pageNum="+params.pagenum+"&orderId="+params.id+"&orderState="+params.state)
}
// 平台退款
export const ordertui=(params)=>{
  return axios({
    url: "/order/order/refundPlat",
    method: "put",
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/x-www-form-urlencoded'
    },
    transformRequest: [
      function(oldData){
        // console.log(oldData)
        let newStr = ''
        for (let item in oldData){
          newStr += encodeURIComponent(item) + '=' + encodeURIComponent(oldData[item]) + '&'
        }
        newStr = newStr.slice(0, -1)
        return newStr
      }
    ],
  }).then(res=>res.data)
}


