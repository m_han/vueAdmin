import axios from 'axios';

// 登录请求方法
const loginreq = (method, url, params) => {
    return axios({
        method: method,
        url: url,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: params,
    }).then(res => res.data);
};
// 通用公用方法
const req = (method, url, params) => {
    return axios({
        method: method,
        url: url,
        data: params,
    }).then(res => res.data);
};

// put提交
const putdata=(url,params)=>{
  return axios({
    method: "put",
    url: url,
    data: params,
    headers: {
      "Authorization": 'Bearer '+localStorage.getItem("logintoken"),
      "Content-Type": 'application/json'
    }
  }).then(res=>res.data)
}

export {
    loginreq,
    req,
    putdata,
}
